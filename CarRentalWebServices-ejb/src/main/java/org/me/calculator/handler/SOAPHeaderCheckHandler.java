/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator.handler;

import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author FER
 */
public class SOAPHeaderCheckHandler implements SOAPHandler<SOAPMessageContext> {

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger("SecurityCheckHandler");

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        boolean isSuccessful = false;
        LOGGER.info("handle Message wurde aufgerufen!");

        boolean direction = ((Boolean) context.get(SOAPMessageContext.MESSAGE_OUTBOUND_PROPERTY)).booleanValue();
        if (direction) {
            LOGGER.info("direction = outbound");
        } else {
            LOGGER.info("direction = inbound");
            handleRequest(context);
        }

        return true;
    }

    public boolean handleRequest(SOAPMessageContext context) {
        LOGGER.info("handleRequest aufgetreten!");
        try {
            final SOAPMessage message = context.getMessage();
            final SOAPBody body = message.getSOAPBody();
            final SOAPHeader header = message.getSOAPHeader();

            final Node multiplySecured = (Node) body.getFirstChild();
            final Node factor1 = (Node) multiplySecured.getFirstChild();
            final Node factor2 = (Node) factor1.getNextSibling();

            LOGGER.info(multiplySecured.getNodeName());
            LOGGER.info(factor1.getNodeName());
            LOGGER.info(factor2.getNodeName());

            final Node Security = (Node) header.getFirstChild();
            LOGGER.info(Security.getNodeName());

            final Node UsernameToken = (Node) Security.getFirstChild();
            final Node Username = (Node) UsernameToken.getFirstChild();
            final Node Password = (Node) Username.getNextSibling();

            String username = Username.getTextContent();
            String password = Password.getTextContent();
            LOGGER.info("Username: " + username);
            LOGGER.info("Password: " + password);

            checkCredentials(username, password, context);

            return true;
        } catch (Exception e) {
            LOGGER.info("Exception aufgetreten!");
            handleFault(context);
            return false;
        }
    }

    public boolean checkCredentials(String username, String password, SOAPMessageContext context) {
        if (username.equals("admin") && password.equals("admin")) {
            return true;
        } else {
            handleFault(context);
            return false;
        }
    }

    public boolean handleResponse(SOAPMessageContext context) {
        return false;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        LOGGER.info("handleFault aufgerufen!");
        throw new UnsupportedOperationException("Invalid Credentials supplied!"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close(MessageContext context) {
        //TODO: implement logic here
        LOGGER.info("close aufgerufen!");
    }
}
