/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator.handler;

import java.util.Set;
import com.sun.istack.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import org.me.calculator.exceptions.InvalidCredentialsException;

/**
 *
 * @author FER
 */
public class SecurityCheckHandler implements SOAPHandler<SOAPMessageContext> {

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger("SecurityCheckHandler");

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        boolean isSuccessful = false;
        LOGGER.info("handle Message wurde aufgerufen!");

        boolean direction = ((Boolean) context.get(SOAPMessageContext.MESSAGE_OUTBOUND_PROPERTY)).booleanValue();
        if (direction) {
            LOGGER.info("direction = outbound");
            isSuccessful = dumpMessage(context);
        } else {
            LOGGER.info("direction = inbound");
            isSuccessful = handleRequest(context);
        }

        return isSuccessful;
    }

    public boolean handleRequest(SOAPMessageContext context) {
        try {
            final SOAPMessage message = context.getMessage();
            final SOAPBody body = message.getSOAPBody();

            Node calculateSum = (Node) body.getFirstChild();
            LOGGER.info(calculateSum.getLocalName());

            Node firstnumber = (Node) calculateSum.getFirstChild();
            Node secondnumber = (Node) firstnumber.getNextSibling();
            Node username = (Node) secondnumber.getNextSibling();
            Node password = (Node) username.getNextSibling();

            String strusername = username.getTextContent();
            String strpassword = password.getTextContent();

            LOGGER.info("firstnumber: " + firstnumber.getTextContent());
            LOGGER.info("secondnumber: " + secondnumber.getTextContent());
            LOGGER.info("username: " + strusername);
            LOGGER.info("password: " + strpassword);
            
            return checkCredentials(strusername, strpassword,context);

            
        } catch (SOAPException e) {
            LOGGER.info("Exception aufgetreten!");
            return false;
        }
    }
    
    public boolean checkCredentials(String username,String password,SOAPMessageContext context){
        if (username.equals("admin") && password.equals("admin")){
            return true;
        }else{
            handleFault(context);
            return false;
        }
    }

    public boolean handleResponse(SOAPMessageContext context) {
        return false;
    }

    private boolean dumpMessage(SOAPMessageContext context) {
        try {
            final SOAPMessage message = context.getMessage();
            final SOAPBody body = message.getSOAPBody();
            final String localName = body.getFirstChild().getLocalName();
            LOGGER.info(localName);
            return true;
        } catch (SOAPException e) {
            LOGGER.info("Exception aufgetreten!");
            return false;
        }
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        LOGGER.info("handleFault aufgerufen!");
        throw new UnsupportedOperationException("Invalid Credentials supplied!"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close(MessageContext context) {
        //TODO: implement logic here
        LOGGER.info("close aufgerufen!");
    }

}
