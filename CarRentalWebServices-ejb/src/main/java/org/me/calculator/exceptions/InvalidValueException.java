/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator.exceptions;

/**
 *
 * @author FER
 */
public class InvalidValueException extends IllegalArgumentException {
    public InvalidValueException(){
        
    }
    
    public InvalidValueException(String message){
        super(message);
    }
}
