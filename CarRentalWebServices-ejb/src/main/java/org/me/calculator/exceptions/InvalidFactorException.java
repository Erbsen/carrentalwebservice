/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator.exceptions;

import javax.xml.ws.WebFault;
import org.me.calculator.faults.InvalidValueFault;

/**
 *
 * @author FER
 */
@WebFault(name="InvalidValueFault")
public class InvalidFactorException extends Exception {
    public InvalidFactorException(){
        
    }
    
    public InvalidFactorException(String message){
        super(message);
    }
    
    public InvalidFactorException(InvalidValueFault invalidValueFault){
        this.invalidValueFault=invalidValueFault;
    }
    
    private InvalidValueFault invalidValueFault;
    
    public InvalidValueFault getFaultInfo(){
        return this.invalidValueFault;
    }
}
