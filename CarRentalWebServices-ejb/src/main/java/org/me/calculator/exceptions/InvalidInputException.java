/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator.exceptions;

import com.sun.xml.ws.security.soap12.Faultcode;
import javax.xml.namespace.QName;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

/**
 *
 * @author FER
 */
public class InvalidInputException extends SOAPFaultException {

    public InvalidInputException(QName faultcode, String faultstring, String faultactor, Detail faultdetail) {
        super(faultcode, faultstring, faultactor, faultdetail);
    }

}
