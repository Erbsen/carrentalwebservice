/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator;


import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebResult;

/**
 *
 * @author FER
 */
@WebService(serviceName = "CaluclatorWSHandler")
@Stateless()
@HandlerChain(file = "CaluclatorWSHandler_handler.xml")
public class CaluclatorWSHandler {
    
    
    private static final Logger LOGGER=Logger.getLogger("CaluclatorWSHandler");

    @WebMethod(operationName = "calculateSum")
    @WebResult(name = "sum")
    public int calculateSum(@WebParam(name = "firstnumber") int firstnumber, @WebParam(name = "secondnumber") int secondnumber,@WebParam(name = "username")String username,@WebParam(name = "password")String password) {
        LOGGER.info("calculateSum was invoked!");
        return firstnumber + secondnumber;
    }
}
