/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.calculator;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.WebResult;
import javax.xml.namespace.QName;
import javax.xml.soap.Detail;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import org.me.calculator.exceptions.InvalidFactorException;
import org.me.calculator.exceptions.InvalidInputException;
import org.me.calculator.exceptions.InvalidSummandException;
import org.me.calculator.exceptions.InvalidValueException;
import org.me.calculator.faults.InvalidValueFault;

/**
 *
 * @author FER
 */
@WebService(serviceName = "CalculatorWS")
@Stateless
public class CalculatorWS {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    @WebMethod(operationName = "add")
    @WebResult(name = "sum")
    public int addPositiveNumbers(@WebParam(name = "number1") int number1,
            @WebParam(name = "number2") int number2)
            throws InvalidSummandException {

        if (number1 > 0 && number2 > 0) {
            return number1 + number2;
        } else {
            throw new InvalidSummandException("summand could not be lower than 0");
        }
    }

    @WebMethod(operationName = "subtract")
    @WebResult(name = "difference")
    public int subtractPositiveNumbers(@WebParam(name = "number1") int number1,
            @WebParam(name = "number2") int number2) {
        if (number1 > 0 && number2 > 0) {
            return number1 - number2;
        } else {
            throw new InvalidValueException("summand could not be lower than 0");
        }
    }

    @WebMethod(operationName = "multiply")
    @WebResult(name = "product")
    public int multiplyPositiveNumbers(@WebParam(name = "number1") int number1,
            @WebParam(name = "number2") int number2) throws InvalidFactorException {
        if (number1 > 0 && number2 > 0) {
            return number1 * number2;
        } else {
            InvalidValueFault invalidValueFault = new InvalidValueFault();
            invalidValueFault.setFaultCode("1-458-8");
            invalidValueFault.setFaultString("The first factor is invalid!");
            throw new InvalidFactorException(invalidValueFault);
        }
    }

    @WebMethod(operationName = "division")
    @WebResult(name = "division")
    public double dividePositiveNumbers(@WebParam(name = "dividenumber1") int number1, @WebParam(name = "dividenumber2") int number2) throws Exception {
        if (number1 > 0 && number2 > 0) {
            return number1 / number2;
        } else {
            QName qname = new QName("http://schemas.xmlsoap.org/soap/envelope/", "Server");
            SOAPFactory fac = SOAPFactory.newInstance();
            SOAPFault sf = fac.createFault(
                    "Meine Fehlermeldung",
                    new QName("http://schemas.xmlsoap.org/soap/envelope/", "Server"));
            Detail d = sf.addDetail();
            SOAPElement de = d.addChildElement(new QName("http://www.example.com/faults", "inputMessageValidationFault"));
            de.addAttribute(new QName("", "msg"), "invalid numbers");
            throw new SOAPFaultException(sf);
        }

    }

    @WebMethod(operationName = "multiplyNegativeNumbers")
    @WebResult(name = "result")
    public int multiplyNegativeNumbers(@WebParam(name = "number1")int number1, @WebParam(name = "number2")int number2) throws SOAPException {
        if (number1 <= 0 && number2 <= 0) {
            return number1 * number2;
        } else {
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            SOAPFault soapFault = soapFactory.createFault("both numbers must be negative!", new QName("http://schemas.xmlsoap.org/soap/envelope/", "Client"));
            Detail detail = soapFault.addDetail();
            SOAPElement de = detail.addChildElement(new QName("http://www.example.com/faults", "inputMessageValidationFault"));

            detail.addAttribute(new QName("", "msg"), "both numbers must be negative!");
            
            throw new SOAPFaultException(soapFault);   
            
        }
    }
}
