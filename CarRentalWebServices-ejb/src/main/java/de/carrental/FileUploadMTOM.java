/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.carrental;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.MTOM;

/**
 *
 * @author FER
 */
@WebService(serviceName = "FileUploadMTOM")
@MTOM
@Stateless()
public class FileUploadMTOM {

    private String Dir = "C:/FileService/";

    public void upload(@WebParam(name = "uploadfilename") String filename, @WebParam(name = "data") byte[] data) {
        try {
            String path = Dir + filename;
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(path));
            out.write(data);
            out.flush();
            out.close();
        } catch (IOException e) {
            throw new WebServiceException("Upload failed!");
        }
    }

    public byte[] download(@WebParam(name = "downloadfilename") String filename) throws FileNotFoundException {
        String path = Dir + filename;
        File file = new File(path);
        
        if (!file.exists()) {
            throw new FileNotFoundException("File does not exist");
        } else {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(path));
                byte[] buffer = new byte[2048];
                int n = 0;
                while ((n = in.read(buffer)) != -1) {
                    out.write(buffer, 0, n);
                }
                in.close();
                return out.toByteArray();
            } catch (Exception e) {
                throw new WebServiceException("Download failed!");
            }
        }
    }
}
