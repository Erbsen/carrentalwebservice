/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.carrental;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebResult;

/**
 *
 * @author FER
 */
@WebService(serviceName = "SecuredWSHeader")
@Stateless()
@HandlerChain(file = "SecuredWSHeader_handler.xml")
public class SecuredWSHeader {

    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger("SecurityCheckHandler");

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "mulitplySecured")
    @WebResult(name = "thresult")
    public int multiplySecured(@WebParam(name = "factor1") int factor1, @WebParam(name = "factor2") int factor2) {
        LOGGER.info("multiplySecured aufgerufen!");
        
        return factor1 * factor2;
    }
}
